Rails.application.routes.draw do
    devise_for :users
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    devise_scope :user do
        authenticated :user do
            root to: 'dashboards#index'

            resources :dashboards, only: %i[index]
            get '/news', to: 'dashboards#news', as: :dashboard_news
            get '/library', to: 'dashboards#library', as: :dashboard_library
            get '/new', to: 'dashboards#new', as: :dashboard_new

            resources :users, only: %i[index show edit update] do
                resources :user_bookmarks, only: %i[index]
            end

            resources :books, only: %i[index show new create edit update destroy] do
                get '/read', to: 'books#read', as: :read
                get '/section/:section_id/read', to: 'books#read_section', as: :read_section
                resources :book_translators, only: %i[new create destroy]
                resources :book_authors, only: %i[new create destroy]
                resources :book_genres, only: %i[new create destroy]

                resources :book_sections, only: %i[index new create edit update destroy] do
                    get '/list', to: 'book_sections#list', as: :edit_list
                    resources :translated_texts, only: %i[create show update destroy]
                    resources :materials, only: %i[new create update destroy]
                end
            end

            resources :original_images, only: %i[update]
            resources :original_texts, only: %i[update]

            resources :user_bookmarks, only: %i[create destroy]

            resources :book_ratings, only: %i[create destroy]
        end

        unauthenticated :user do
            root to: 'devise/sessions#new'
        end
    end
end
