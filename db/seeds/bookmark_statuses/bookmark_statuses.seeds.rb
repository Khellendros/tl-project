DatabaseCleaner.clean_with(:truncation, only: %w(bookmark_statuses))
seed_file = Rails.root.join('db', 'seeds/bookmark_statuses', 'bookmark_statuses.seeds.yml')
data = YAML.load_file(seed_file)

p 'bookmark_statuses...'
BookmarkStatus.create!(data)
