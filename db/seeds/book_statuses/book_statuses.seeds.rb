DatabaseCleaner.clean_with(:truncation, only: %w(book_statuses))
seed_file = Rails.root.join('db', 'seeds/book_statuses', 'book_statuses.seeds.yml')
data = YAML.load_file(seed_file)

p 'book_statuses...'
BookStatus.create!(data)
