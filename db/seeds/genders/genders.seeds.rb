DatabaseCleaner.clean_with(:truncation, only: %w(genders))
seed_file = Rails.root.join('db', 'seeds/genders', 'genders.seeds.yml')
data = YAML.load_file(seed_file)

p 'genders...'
Gender.create!(data)
