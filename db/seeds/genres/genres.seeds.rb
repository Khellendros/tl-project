DatabaseCleaner.clean_with(:truncation, only: %w(genres))
seed_file = Rails.root.join('db', 'seeds/genres', 'genres.seeds.yml')
data = YAML.load_file(seed_file)

p 'genres...'
Genre.create!(data)
