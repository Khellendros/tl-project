DatabaseCleaner.clean_with(:truncation, only: %w(roles))
seed_file = Rails.root.join('db', 'seeds/roles', 'roles.seeds.yml')
data = YAML.load_file(seed_file)

p 'roles...'
Role.create!(data)
