DatabaseCleaner.clean_with(:truncation, only: %w(section_statuses))
seed_file = Rails.root.join('db', 'seeds/section_statuses', 'section_statuses.seeds.yml')
data = YAML.load_file(seed_file)

p 'section_statuses...'
SectionStatus.create!(data)
