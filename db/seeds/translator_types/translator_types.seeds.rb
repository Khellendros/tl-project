DatabaseCleaner.clean_with(:truncation, only: %w(translator_types))
seed_file = Rails.root.join('db', 'seeds/translator_types', 'translator_types.seeds.yml')
data = YAML.load_file(seed_file)

p 'translator_types...'
TranslatorType.create!(data)
