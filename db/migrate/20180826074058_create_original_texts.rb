class CreateOriginalTexts < ActiveRecord::Migration[5.2]
    def change
        create_table :original_texts do |t|
            t.integer :position
            t.text :text

            t.timestamps
        end
    end
end
