class AddImagesToBookAndCharacter < ActiveRecord::Migration[5.2]
    def change
        add_attachment :books, :covers
        add_attachment :users, :avatars
    end
end
