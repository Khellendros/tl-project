class AddBookSectionToOriginalMaterials < ActiveRecord::Migration[5.2]
    def change
        add_column :original_texts, :book_section_id, :integer
        add_column :original_images, :book_section_id, :integer
    end
end
