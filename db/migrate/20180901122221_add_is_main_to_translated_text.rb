class AddIsMainToTranslatedText < ActiveRecord::Migration[5.2]
    def change
        add_column :translated_texts, :is_main, :boolean, null: false, default: false
    end
end
