class AddFieldsForUser < ActiveRecord::Migration[5.2]
    def change
        add_column :users, :gender_id, :integer
        add_column :users, :birthday, :date
    end
end
