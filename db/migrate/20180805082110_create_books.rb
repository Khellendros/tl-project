class CreateBooks < ActiveRecord::Migration[5.2]
    def change
        create_table :books do |t|
            t.integer :user_id
            t.integer :author_id
            t.string :title
            t.text :annotation
            t.boolean :is_private, default: false, null: false

            t.timestamps
        end
    end
end
