class AddAtarToCharacter < ActiveRecord::Migration[5.2]
    def change
        add_attachment :characters, :avatars
    end
end
