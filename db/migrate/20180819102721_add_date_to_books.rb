class AddDateToBooks < ActiveRecord::Migration[5.2]
    def change
        add_column :books, :start_date, :date
        add_column :books, :end_date, :date
        add_column :books, :book_status_id, :integer
    end
end
