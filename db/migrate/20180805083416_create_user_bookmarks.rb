class CreateUserBookmarks < ActiveRecord::Migration[5.2]
    def change
        create_table :user_bookmarks do |t|
            t.integer :user_id
            t.integer :book_id
            t.integer :bookmark_status_id

            t.timestamps
        end
    end
end
