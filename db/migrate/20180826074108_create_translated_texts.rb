class CreateTranslatedTexts < ActiveRecord::Migration[5.2]
    def change
        create_table :translated_texts do |t|
            t.integer :original_text_id
            t.integer :user_id

            t.text :text

            t.timestamps
        end
    end
end
