class CreateBookSections < ActiveRecord::Migration[5.2]
    def change
        create_table :book_sections do |t|
            t.string :title
            t.integer :book_id
            t.integer :position
            t.integer :section_status_id, null: false, default: 1

            t.timestamps
        end
    end
end
