class CreateBookTranslators < ActiveRecord::Migration[5.2]
    def change
        create_table :book_translators do |t|
            t.integer :book_id
            t.integer :user_id
            t.integer :translator_type_id

            t.timestamps
        end
    end
end
