class CreateBookCharacters < ActiveRecord::Migration[5.2]
    def change
        create_table :book_characters do |t|
            t.integer :book_id
            t.integer :character_id

            t.timestamps
        end
    end
end
