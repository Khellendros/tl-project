class CreateOriginalImages < ActiveRecord::Migration[5.2]
    def change
        create_table :original_images do |t|
            t.integer :position

            t.timestamps
        end
    end
end
