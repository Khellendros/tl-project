class CreateMaterials < ActiveRecord::Migration[5.2]
    def change
        create_table :materials do |t|
            t.integer :book_section_id
            t.integer :position

            t.integer :materialable_id
            t.string :materialable_type

            t.timestamps
        end
    end
end
