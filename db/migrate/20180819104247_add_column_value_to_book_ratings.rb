class AddColumnValueToBookRatings < ActiveRecord::Migration[5.2]
    def change
        add_column :book_ratings, :value, :integer
    end
end
