class AddAttachmentToOriginalImage < ActiveRecord::Migration[5.2]
    def change
        add_attachment :original_images, :images
    end
end
