# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rake::Task['db:seed:bookmark_statuses'].invoke

Rake::Task['db:seed:genres'].invoke

Rake::Task['db:seed:genders'].invoke

Rake::Task['db:seed:translator_types'].invoke

Rake::Task['db:seed:book_statuses'].invoke

Rake::Task['db:seed:section_statuses'].invoke

Rake::Task['db:seed:roles'].invoke
