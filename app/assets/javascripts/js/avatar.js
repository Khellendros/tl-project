function change_avatar() {
    $("#for_user_avatars").click();
    $("#user_avatars").off("change")
    $("#user_avatars").on("change", function(e) {
        readAvatar(this);
    })
}

function readAvatar(input) {
    let mime_types = [
        'image/png',
        'image/jpg',
        'image/jpeg',
        'image/bmp'
    ]
    let img = $("#avatar_image")

    if (input.files && input.files[0]) {
        if (mime_types.indexOf(input.files[0].type) > -1) {
            let reader = new FileReader();
            reader.onload = function(e) {
                let image = new Image()
                image.src = e.target.result
                img.attr('src', image.src);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            img.hide()
            $(input).val("")
        }
    }
}
