function change_cover(el) {
    $("#"+el).click()
}

function delete_cover(el) {
    let input = $(el).parents(".dropzone").find("input")
    $(input).val("").change()
}

function send_form() {
    $('.dropdown-trigger').dropdown();

    $(".ajax-text").off("change").on("change", function(e) {
        let formData = new FormData(e.currentTarget.form);
        $.ajax({
            url: e.currentTarget.form.action,
            async: true,
            type: e.currentTarget.form.method,
            data: formData,
            // dataType: 'application/json',
            processData: false,
            contentType: false,
            success: function(result) {
                // console.log(result)
            },
            error: function(result) {
                // console.log(result)
            }
        })
    })

    $(".ajax-cover").off("change").on("change", function(e) {
        let formData = new FormData(e.currentTarget.form);

        let mime_types = [
            'image/png',
            'image/jpg',
            'image/jpeg',
            'image/bmp'
        ]

        let img = $(e.currentTarget).siblings("img.image_preview")
        let dynamic_data = $(e.currentTarget).siblings(".dynamic-data")
        let buttons_block = $(e.currentTarget).siblings(".buttons-block")

        if (e.currentTarget.files && e.currentTarget.files[0]) {
            if (mime_types.indexOf(e.currentTarget.files[0].type) > -1) {
                let reader = new FileReader();
                let image_result = ""
                reader.onload = function(el) {
                    let image = new Image()
                    image.onload = function() {

                    }
                    image.src = el.target.result
                    image_result = image.src
                }
                reader.readAsDataURL(e.currentTarget.files[0]);

                $.ajax({
                    async: true,
                    method: 'PUT',
                    url: e.currentTarget.form.action,
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        let xhr = $.ajaxSettings.xhr();
                        xhr.upload.onprogress = function(event) {
                            let count = parseInt((100 * event.loaded) / event.total)
                            console.log(count)
                        }
                        xhr.upload.onloadstart = function(event) {
                            let html = ""
                                html += '<div class="preloader valign-wrapper">'
                                  html += '<div class="preloader-wrapper big active">'
                                    html += '<div class="spinner-layer spinner-blue-grey-darken-1-only">'
                                      html += '<div class="circle-clipper left">'
                                        html += '<div class="circle"></div>'
                                      html += '</div>'
                                      html += '<div class="gap-patch">'
                                        html += '<div class="circle"></div>'
                                      html += '</div>'
                                      html += '<div class="circle-clipper right">'
                                        html += '<div class="circle"></div>'
                                      html += '</div>'
                                    html += '</div>'
                                  html += '</div>'
                                html += '</div>'

                            dynamic_data.html(html)
                            buttons_block.hide()
                        }
                        return xhr;
                    },
                    success: function() {
                        let html = ""
                            html += "<img class='image_preview' src='" + image_result + "' onclick='change_cover(\""+e.currentTarget.id+"\");'>"
                        dynamic_data.html(html)
                        buttons_block.css("right", "15px").show()
                    }

                })
            }
        }
    })
}
