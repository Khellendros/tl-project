function dropZone() {
    $(".dropzone").on("drag dragstart dragend dragover dragenter dragleave drop", function(e) {
        e.preventDefault();
        e.stopPropagation();
    }).on('drop', function(e) {
        let droppedFiles = e.originalEvent.dataTransfer.files;
        $(this).children("input[type='file']").prop('files', droppedFiles);
    });
    $("input[type='file']").on("change", function(e) {
        readURL(this);
    })
    $('.dropdown-trigger').dropdown();
}

function readURL(input) {
    let mime_types = [
        'image/png',
        'image/jpg',
        'image/jpeg',
        'image/bmp'
    ]
    let img = $(input).siblings("img.image_preview")
    let propzone_el = $(input).parents(".dropzone")
    let button_more = $(input).siblings(".buttons-block")
    let dropzone_info = $(input).siblings(".dropzone-info")
    if (input.files && input.files[0]) {
        if (mime_types.indexOf(input.files[0].type) > -1) {
            let reader = new FileReader();
            reader.onload = function(e) {
                let image = new Image()
                image.onload = function() {

                    if (image.width < propzone_el.width()) {
                        let offset = (propzone_el.width() - image.width) / 2 + 15
                        button_more.css("right", offset)
                    } else {
                        button_more.css("right", "15px")
                    }
                }
                image.src = e.target.result

                dropzone_info.css("cssText", "display: none !important;")

                button_more.css("display", "block")
                propzone_el.css("border", "none").css("height", "auto")
                img.attr('src', e.target.result);
                img.show()
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            img.hide()
            $(input).val("")
            dropzone_info.css("cssText", "display: flex !important;")
            button_more.hide().css("right", "15px")
            propzone_el.css("border", "2px dashed #546E7A").css("height", "300px")
        }
    }
}

function another_load(el) {
    $(el).parents(".dropzone").find("label").click()
}

function remove_load(el) {
    let input = $(el).parents(".dropzone").find("input")
    let img = $(input).siblings("img.image_preview")
    let propzone_el = $(input).parents(".dropzone")
    let button_more = $(input).siblings(".buttons-block")
    let dropzone_info = $(input).siblings(".dropzone-info")

    img.hide()
    $(input).val("")
    dropzone_info.show()
    button_more.hide().css("right", "15px")
    propzone_el.css("border", "2px dashed #546E7A").css("height", "300px")
}
