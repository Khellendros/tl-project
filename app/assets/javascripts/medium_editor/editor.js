function initEditor(element = '.editor') {
    var elements = document.querySelectorAll(element)
    if (elements.length == 0) {
        return
    }

    var editor = new MediumEditor(element, {
        toolbar: {
            allowMultiParagraphSelection: true,
            buttons: [{
                name: "bold",
                contentDefault: "<div class = 'custom-icon'><div class = 'icon-bold'></div><span>Жирный</span></div>",
                aria: "Жирный"
            }, {
                name: "italic",
                contentDefault: "<div class = 'custom-icon'><div class = 'icon-italic'></div><span>Курсив</span></div>",
                aria: "Курсив"
            }, {
                name: "underline",
                contentDefault: "<div class = 'custom-icon custom-icon-underline'><div class = 'icon-underline'></div><span>Подчеркнутый</span></div>",
                aria: "Подчеркнутый"
            }, {
                name: "h5",
                contentDefault: "<div class = 'custom-icon custom-icon-header'><div class = 'icon-header'></div><span>Заголовок</span></div>",
                aria: "Заголовок"
            }
            , {
                name: "justifyLeft",
                // contentDefault: "<div class = 'custom-icon custom-icon-header'><div class = 'icon-left-align'></div><span>Слева</span></div>",
                aria: "Слева"
            }, {
                name: "justifyCenter",
                // contentDefault: "<div class = 'custom-icon custom-icon-header'><div class = 'icon-center-align'></div><span>По центру</span></div>",
                aria: "По центру"
            }, {
                name: "justifyRight",
                // contentDefault: "<div class = 'custom-icon custom-icon-header'><div class = 'icon-right-align'></div><span>Справа</span></div>",
                aria: "Справа"
            }, {
                name: "justifyFull",
                // contentDefault: "<div class = 'custom-icon custom-icon-header'><div class = 'icon-justify-align'></div><span>По ширине</span></div>",
                aria: "По ширине"
            }
          ],
            diffLeft: 0,
            diffTop: -10,
            firstButtonClass: 'medium-editor-button-first',
            lastButtonClass: 'medium-editor-button-last',
            relativeContainer: null,
            standardizeSelectionStart: false,
            static: false,
            align: 'center',
            sticky: false,
            updateOnEmptySelection: false
        },
        anchor: {
            targetBlank: true,
            linkValidation: true,
            placeholderText: 'Введите ссылку...'
        },
        placeholder: {
            text: 'Введите текст...',
            hideOnClick: true
        },
        keyboardCommands: false,
        imageDragging: false
    }).subscribe('editableInput', function(event, editorElement) {
        var origin_name = $(editorElement).attr("name")
        if ($(editorElement).html() == "<p><br></p>") {
            $("textarea[name='" + origin_name + "']").val("")
        }
    }).subscribe('blur', function (event, editorElement) {
        $(editorElement).parents('form').submit()
    });

}
