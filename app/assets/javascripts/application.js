// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require popper
//= require bootstrap

//= require jquery_ujs
//= require activestorage
//= require turbolinks
//= require_tree .

$(document).on('submit', 'form[data=turbolinks-form]', function(e) {
    var options = {};
    Turbolinks.visit(
        this.action + (this.action.indexOf('?') === -1 ? '?' : '&') + $(this).serialize(),
        options
    );
    return false;
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// document.addEventListener('turbolinks:load', function() {
//    $('body').bootstrapMaterialDesign();
//    //= require turbolinks
// })
//
// document.addEventListener('turbolinks:load', function() {
//
//     var elem = document.querySelector('#mobile-nav');
//     if (elem) {
//         var instance = new M.Sidenav(elem, {});
//     }
//
//     var elem2 = document.querySelector('.dropdown-trigger');
//     if (elem2) {
//         var instance2 = new M.Dropdown(elem2, {});
//     }
//
//     var elems3 = document.querySelectorAll('select');
//     if (elems3) {
//         elems3.forEach(function(elem3) {
//             var instance3 = new M.FormSelect(elem3, {});
//         })
//     }
//
//     var elems4 = document.querySelectorAll('.datepicker');
//     if (elems4) {
//         var datapicker_options = {
//             i18n: {
//                 months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
//                 monthsShort: ['Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Ноябр', 'Дек'],
//                 weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
//                 weekdaysShort: ['Воскр', 'Пон', 'Вт', 'Ср', 'Четв', 'Пятн', 'Субб'],
//                 weekdaysAbbrev: ['Вс', 'Пп', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
//             },
//             yearRange: 20,
//             minYear: 1960,
//             maxYear: 2018
//         }
//         elems4.forEach(function(elem4) {
//             var instance4 = new M.Datepicker(elem4, datapicker_options);
//         })
//     }
//
//     var elem5 = document.querySelector('.tabs');
//     if (elem5) {
//         var instance5 = new M.Tabs(elem5, {});
//     }
//
//     var elems6 = document.querySelectorAll('.tooltipped');
//     if (elems6) {
//         elems6.forEach(function(elem6) {
//             var instance6 = new M.Tooltip(elem6, {});
//         })
//     }
//
//     var elems7 = document.querySelectorAll('.modal');
//     if (elems7) {
//         elems7.forEach(function(elem7) {
//             var instance7 = M.Modal.init(elem7, {});
//         })
//     }
//
//     // var elems8 = document.querySelectorAll('.autocomplete');
//     // if (elems8) {
//     //     elems8.forEach(function(elem8) {
//     //         var instance8 = M.Autocomplete.init(elem8, {});
//     //     })
//     // }
//
//     var elems9 = document.querySelectorAll('.carousel');
//     var instances = M.Carousel.init(elems9, {});
// });
//
// document.addEventListener('turbolinks:before-visit', function() {
//
//     var elem = document.querySelector('#mobile-nav');
//     if (elem) {
//         var instance = M.Sidenav.getInstance(elem);
//         if (instance) {
//             instance.destroy();
//         }
//     }
//
//     var elems2 = document.querySelectorAll('.dropdown-trigger');
//     if (elems2) {
//         elems2.forEach(function(elem2) {
//             var instance2 = M.Dropdown.getInstance(elem2);
//             if (instance2) {
//                 instance2.destroy();
//             }
//         })
//     }
//
//     var elems3 = document.querySelectorAll('select');
//     if (elems3) {
//         elems3.forEach(function(elem3) {
//             var instance3 = M.FormSelect.getInstance(elem3);
//             if (instance3) {
//                 instance3.destroy();
//             }
//         })
//     }
//
//     var elems4 = document.querySelectorAll('.datepicker');
//     if (elems4) {
//         elems4.forEach(function(elem4) {
//             var instance4 = M.Datepicker.getInstance(elem4);
//             if (instance4) {
//                 instance4.destroy();
//             }
//         })
//     }
//
//     var elem5 = document.querySelector('.tabs');
//     if (elem5) {
//         var instance5 = M.Tabs.getInstance(elem5);
//         if (instance5) {
//             instance5.destroy();
//         }
//     }
//
//     var elems6 = document.querySelectorAll('.tooltipped');
//     if (elems6) {
//         elems6.forEach(function(elem6) {
//             var instance6 = M.Tooltip.getInstance(elem6);
//             if (instance6) {
//                 instance6.destroy();
//             }
//         })
//     }
//
//     var elems7 = document.querySelectorAll('.modal');
//     if (elems7) {
//         elems7.forEach(function(elem7) {
//             var instance7 = M.Modal.getInstance(elem7);
//             if (instance7) {
//                 instance7.destroy();
//             }
//         })
//     }
//
//     var elems8 = document.querySelectorAll('.autocomplete');
//     if (elems8) {
//         elems8.forEach(function(elem8) {
//             var instance8 = M.Autocomplete.getInstance(elem8);
//             if (instance8) {
//                 instance8.destroy();
//             }
//         })
//     }
//
//     var elems9 = document.querySelectorAll('.carousel');
//     if (elems9) {
//         elems9.forEach(function(elem9) {
//             var instance9 = M.Carousel.getInstance(elem9);
//             if (instance9) {
//                 instance9.destroy();
//             }
//         })
//     }
// });
