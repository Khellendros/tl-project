module ApplicationHelper
    def get_translator_type(_user_id, _book_id)
        BookTranslator.find_by(book_id: _book_id, user_id: _user_id).translator_type
    end

    def get_book_author_id(_book_id, _author_id)
        BookAuthor.find_by(book_id: _book_id, author_id: _author_id).id
    end

    def get_book_genre_id(_book_id, _genre_id)
        BookGenre.find_by(book_id: _book_id, genre_id: _genre_id).id
    end

    def get_book_translator_id(_book_id, _translator_id)
        BookTranslator.find_by(book_id: _book_id, user_id: _translator_id).id
    end

    def bookmark_status(_book_id)
        status = 'Добавить'
        bookmark = UserBookmark.find_by(user_id: current_user.id, book_id: _book_id)
        status = bookmark.bookmark_status.name unless bookmark.nil?
        status
    end

    def get_book_dates(_book)
        start_date = _book.start_date
        end_date = _book.end_date
        text = ''
        # text += "c #{start_date.nil? ? '??' : start_date.year}"
        # text += " по #{end_date.nil? ? '??' : end_date.year}"
        text
    end

    def get_book_rating(_book_id)
        rating = 0
        book_rating = BookRating.find_by(book_id: _book_id, user_id: current_user.id)
        rating = book_rating.value.to_f / 2 unless book_rating.nil?
        rating
    end

    def is_editor_or_creator?(_book_id)
        book_translator = BookTranslator.find_by(book_id: _book_id, user_id: current_user.id)
        book_translator.translator_type_id != 2
    end

    def translation_info(_translated)
        text = ''
        text += "#{_translated.user.nickname}, от #{_translated.created_at.strftime('%H:%M %d-%m-%Y')}"
        text
    end
end
