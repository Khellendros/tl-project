class Gender < ApplicationRecord
    has_many :users, foreign_key: :gender_id

    validates :name, presence: true, on: %i[update create]
end
