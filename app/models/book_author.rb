class BookAuthor < ApplicationRecord
    belongs_to :book, foreign_key: :book_id
    belongs_to :author, foreign_key: :author_id

    validates :book_id, presence: true, on: %i[update create]
    validates :author_id, presence: true, on: %i[update create]

    validates_uniqueness_of :author_id, scope: :book_id, on: %i[update create]

    attr_accessor :author_name

    validate :check_authors_count, on: :create

    private

    def check_authors_count
        authors = BookAuthor.where(book_id: book_id)
        unless authors.blank?
            if authors.count >= 3
                errors.add(:author, :blank, message: 'no more than 3')
            end
        end
    end
end
