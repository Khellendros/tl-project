class OriginalImage < ApplicationRecord
    has_one :material, as: :materialable, dependent: :destroy

    has_attached_file :images, styles: { medium: '300x300>', thumb: '115x150>', cover: '750x' },
                               default_url: '/images/:style/missing.jpeg',
                               path: ':rails_root/public/:class/:id/:style/:basename.:extension',
                               url: '/:class/:id/:style/:basename.:extension',
                               restricted_characters: /[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%#\(\) ]/

    before_post_process { translit_paperclip_file_name images }

    validates_attachment_content_type :images, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp']

    validates :position, presence: true, on: %i[update create]
    validates :book_section_id, presence: true, on: %i[update create]

    after_create_commit :set_position

    validates_uniqueness_of :position, scope: :book_section_id, on: %i[update create]

    validate :check_position_value, on: %i[create update]

    private

    def check_position_value
        materials = BookSection.find(book_section_id).materials
        current_material = materials.find_by(materialable_type: 'OriginalImage', materialable_id: id)

        positions = materials.pluck(:position)

        if current_material.nil?
            errors.add(:position, :uniqueness, message: 'already been taken') if positions.include?(position)
        else
            errors.add(:position, :uniqueness, message: 'already been taken') if positions.include?(position) && current_material.position != position
        end
    end

    def set_position
        sql = ActiveRecord::Base.connection
        sql.execute("
            UPDATE materials
            SET position = '#{position}'
            WHERE id = '#{material.id}'
        ")
    end
end
