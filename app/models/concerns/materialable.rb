module Materialable
    extend ActiveSupport::Concern
    include ActionView::RecordIdentifier
    include ActionView::Helpers

    included do
        has_many :original_texts, through: :materials, source: :materialable, source_type: 'OriginalText'
        has_many :original_images, through: :materials, source: :materialable, source_type: 'OriginalImage'

        accepts_nested_attributes_for :original_texts, allow_destroy: true
        accepts_nested_attributes_for :original_images, allow_destroy: true
    end

    module ClassMethods
        # static methods go here
        # def static_method
        #
        # end
    end
end
