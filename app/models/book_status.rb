class BookStatus < ApplicationRecord
    has_many :books, foreign_key: :book_id

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]
end
