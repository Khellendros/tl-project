class Genre < ApplicationRecord
    has_many :book_genres, foreign_key: :genre_id, dependent: :destroy

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]

    def genre_id
        id
    end
end
