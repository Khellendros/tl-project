class Role < ApplicationRecord
    has_many :user_roles, foreign_key: :role_id, dependent: :destroy

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]
end
