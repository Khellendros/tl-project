class Book < ApplicationRecord
    belongs_to :user, foreign_key: :user_id
    belongs_to :book_status, foreign_key: :book_status_id

    has_many :book_ratings, foreign_key: :book_id, dependent: :destroy

    has_many :user_bookmarks, foreign_key: :book_id, dependent: :destroy
    has_many :book_sections, foreign_key: :book_id, dependent: :destroy

    has_many :book_characters, foreign_key: :book_id, dependent: :destroy, inverse_of: :book
    has_many :characters, -> { order(:id) }, through: :book_characters, source: :character, class_name: 'Character'

    has_many :book_authors, foreign_key: :book_id, dependent: :destroy, inverse_of: :book
    has_many :authors, -> { order(:id) }, through: :book_authors, source: :author, class_name: 'Author'
    accepts_nested_attributes_for :book_authors, allow_destroy: true

    has_many :book_translators, foreign_key: :book_id, dependent: :destroy, inverse_of: :book
    has_many :translators, -> { order(:id) }, through: :book_translators, source: :user, class_name: 'User'
    accepts_nested_attributes_for :book_translators, allow_destroy: true

    has_many :book_genres, foreign_key: :book_id, dependent: :destroy, inverse_of: :book
    has_many :genres, through: :book_genres, source: :genre, class_name: 'Genre'
    accepts_nested_attributes_for :book_genres, allow_destroy: true

    validates :title, presence: true, on: %i[update create]
    validates :annotation, presence: true, on: %i[update create]
    validates :book_status_id, presence: true, on: %i[update create]
    validates :user_id, presence: true, on: %i[update create]

    attr_accessor :change_book_genres

    has_attached_file :covers, styles: { card: 'x180', medium: '300x300>', thumb: '115x150>', cover: '750x' },
                               default_url: '/images/:style/missing.jpeg',
                               path: ':rails_root/public/:class/:id/:style/:basename.:extension',
                               url: '/:class/:id/:style/:basename.:extension',
                               restricted_characters: /[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%#\(\) ]/

    before_post_process { translit_paperclip_file_name covers }

    validates_attachment_content_type :covers, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp']

    def self.new_on_site
        where("created_at BETWEEN '#{7.day.ago}' AND '#{DateTime.now}' AND is_private = false").order("RANDOM()").limit(5)
    end

    def translator_ids
        book_translators.pluck(:user_id)
    end

    def is_creator?(_user_id)
        user_id == _user_id
    end

    def is_translator?(_user_id)
        translator_ids.include?(_user_id)
    end

    def self.library_book(genre_id, order_by)
        books = where(is_private: false)
        unless genre_id.blank?
            books = books.joins("LEFT JOIN (SELECT book_id, genre_id FROM book_genres WHERE genre_id = #{genre_id}) AS genres ON genres.book_id = books.id").where("genre_id IS NOT NULL")
        end
        unless order_by.blank?
            books = books.joins("LEFT JOIN (SELECT book_id, AVG(value) AS rating FROM book_ratings GROUP BY book_id) AS ratings ON ratings.book_id = books.id").order("rating IS NULL, #{order_by} DESC")
        end
        books
    end

    def rating
        value = (book_ratings.sum(:value).to_f / book_ratings.count).round(2)
        if value.nan?
            {
                value: '-',
                text: 'Не оценено'
            }
        else
            {
                value: value,
                text: rating_text(value)
            }
        end
    rescue ZeroDivisionError
        {
            value: '-',
            text: 'Не оценено'
        }
    end

    def in_lists
        sql_text = ''
        sql_text += 'SELECT bookmark_statuses.name, COUNT(user_bookmarks.book_id) FROM user_bookmarks '
        sql_text += 'LEFT JOIN bookmark_statuses ON bookmark_statuses.id = user_bookmarks.bookmark_status_id '
        sql_text += "WHERE user_bookmarks.book_id = #{id} "
        sql_text += 'GROUP BY bookmark_statuses.name'

        bookmarks_count = user_bookmarks.count

        in_lists = ActiveRecord::Base.connection.exec_query(sql_text).to_hash
        in_lists.each do |list|
            list['count_percent'] = list['count'] * 100.to_f / bookmarks_count
            list['other_percent'] = 100 - list['count_percent']
        end

        in_lists
    end

    def in_ratings
        sql_text = ''
        sql_text += 'SELECT book_ratings.value, COUNT(book_ratings.book_id) FROM book_ratings '
        sql_text += "WHERE book_ratings.book_id = #{id} "
        sql_text += 'GROUP BY book_ratings.value'

        ratings_count = book_ratings.count

        in_ratings = ActiveRecord::Base.connection.exec_query(sql_text).to_hash
        in_ratings.each do |rating|
            rating['count_percent'] = rating['count'] * 100.to_f / ratings_count
            rating['other_percent'] = 100 - rating['count_percent']
        end

        in_ratings
    end

    after_create :create_first_translator

    private

    def create_first_translator
        book_translators.create(user_id: user_id, book_id: id, translator_type_id: 1)
    end

    def rating_text(value)
        texts = ['Тут даже говорить не о чем', 'Хуже некуда', 'Ужасно', 'Очень плохо', 'Плохо', 'Более-менее', 'Нормально', 'Хорошо', 'Отлично', 'Великолепно', 'Эпик вин!']
        texts[value.round]
    end
end
