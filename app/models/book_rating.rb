class BookRating < ApplicationRecord
    belongs_to :book, foreign_key: :book_id
    belongs_to :user, foreign_key: :user_id

    validates :book_id, presence: true, on: %i[update create]
    validates :user_id, presence: true, on: %i[update create]
    validates :value, presence: true, on: %i[update create]

    validates_uniqueness_of :book_id, scope: :user_id, on: %i[update create]

    validate :check_rating_value, on: %i[create update]

    def check_rating_value
        valid = false
        if value.is_a? Integer
            valid = true if value.between?(0, 10)
        end
        add.error(:value, :argument_error, message: 'wrong number') unless valid
    end
end
