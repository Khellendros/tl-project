class BookCharacter < ApplicationRecord
    belongs_to :book, foreign_key: :book_id
    belongs_to :character, foreign_key: :character_id

    validates :book_id, presence: true, on: %i[update create]
    validates :character_id, presence: true, on: %i[update create]

    validates_uniqueness_of :book_id, scope: :character_id, on: %i[update create]
end
