class User < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

    has_many :books, foreign_key: :user_id
    has_many :book_ratings, foreign_key: :user_id, dependent: :destroy
    has_many :user_bookmarks, foreign_key: :user_id, dependent: :destroy
    has_many :book_translators, foreign_key: :user_id, dependent: :destroy
    has_many :translated_texts, foreign_key: :user_id, dependent: :destroy
    has_many :user_roles, foreign_key: :user_id, dependent: :destroy

    has_many :translated_books, -> { order(:id) }, through: :book_translators, source: :book, class_name: 'Book'

    belongs_to :gender, foreign_key: :gender_id, optional: true

    has_attached_file :avatars, styles: { medium: '160x160>', thumb: '100x100>' },
                                default_url: '/avatar/missing.jpeg',
                                path: ':rails_root/public/:class/:id/:style/:basename.:extension',
                                url: '/:class/:id/:style/:basename.:extension',
                                restricted_characters: /[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%#\(\) ]/

    before_post_process { translit_paperclip_file_name avatars }

    validates_attachment_content_type :avatars, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp']

    def nickname
        name.blank? ? email.split('@')[0] : name
    end

    def info
        info = ''
        info += "#{gender.name} / " unless gender.nil?
        unless birthday.nil?
            age = ((Date.today - birthday) / 365).floor
            info += "#{age} #{Russian.p(age, 'год', 'года', 'лет')} / "
        end
        date = created_at.year.to_i
        info += "На сайте с #{date} года"
    end

    def books_count
        books.count
    end

    def books_member_count
        book_translators.where.not(translator_type_id: 1).count
    end

    def fragments_count
        translated_texts.count
    end

end
