class Author < ApplicationRecord
    has_many :book_authors, foreign_key: :author_id, dependent: :destroy

    validates :name, presence: true, on: %i[update create]
end
