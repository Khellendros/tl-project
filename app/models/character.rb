class Character < ApplicationRecord
    has_many :book_characters, foreign_key: :character_id, dependent: :destroy

    validates :name, presence: true, on: %i[update create]

    has_attached_file :avatars, styles: { medium: '300x300>', thumb: '100x100>' },
                                default_url: '/images/:style/missing.png',
                                path: ':rails_root/public/:class/:id/:style/:basename.:extension',
                                url: '/:class/:id/:style/:basename.:extension',
                                restricted_characters: /[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%#\(\) ]/

    before_post_process { translit_paperclip_file_name avatars }

    validates_attachment_content_type :avatars, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp']
end
