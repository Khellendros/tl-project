class OriginalText < ApplicationRecord
    has_one :material, as: :materialable, dependent: :destroy
    has_many :translated_texts, foreign_key: :original_text_id, dependent: :destroy

    validates :position, presence: true, on: %i[update create]
    validates :book_section_id, presence: true, on: %i[update create]

    after_create_commit :set_position

    validates_uniqueness_of :position, scope: :book_section_id, on: %i[update create]

    validate :check_position_value, on: %i[create update]

    def main_transalted_text
        translated_text = translated_texts.find_by(is_main: true)
        _text = ''
        _text = translated_text.text unless translated_text.nil?
        _text
    end

    private

    def check_position_value
        materials = BookSection.find(book_section_id).materials
        current_material = materials.find_by(materialable_type: 'OriginalText', materialable_id: id)

        positions = materials.pluck(:position)

        if current_material.nil?
            errors.add(:position, :uniqueness, message: 'already been taken') if positions.include?(position)
        else
            errors.add(:position, :uniqueness, message: 'already been taken') if positions.include?(position) && current_material.position != position
        end
    end

    def set_position
        sql = ActiveRecord::Base.connection
        sql.execute("
            UPDATE materials
            SET position = '#{position}'
            WHERE id = '#{material.id}'
        ")
    end
end
