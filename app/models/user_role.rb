class UserRole < ApplicationRecord
    belongs_to :role, foreign_key: :role_id
    belongs_to :user, foreign_key: :user_id

    validates :user_id, presence: true, on: %i[update create]
    validates :role_id, presence: true, on: %i[update create]

    validates_uniqueness_of :role_id, scope: %i[user_id], on: %i[update create]
end
