class UserBookmark < ApplicationRecord
    belongs_to :user, foreign_key: :user_id
    belongs_to :book, foreign_key: :book_id
    belongs_to :bookmark_status, foreign_key: :bookmark_status_id

    validates :user_id, presence: true, on: %i[update create]
    validates :book_id, presence: true, on: %i[update create]
    validates :bookmark_status_id, presence: true, on: %i[update create]

    validates_uniqueness_of :book_id, scope: :user_id, on: %i[update create]
end
