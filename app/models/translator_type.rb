class TranslatorType < ApplicationRecord
    has_many :book_translators, foreign_key: :translator_type_id

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]
end
