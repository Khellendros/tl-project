class BookGenre < ApplicationRecord
    belongs_to :book, foreign_key: :book_id
    belongs_to :genre, foreign_key: :genre_id

    validates :genre_id, presence: true, on: %i[update create]

    validates_uniqueness_of :book_id, scope: :genre_id, on: %i[update create]

    attr_accessor :genre_name
end
