class Material < ApplicationRecord

    belongs_to :materialable, polymorphic: true, dependent: :destroy

    belongs_to :book_section, foreign_key: :book_section_id

    validates :book_section_id, presence: true, on: %i[update create]

    attr_accessor :material_type
end
