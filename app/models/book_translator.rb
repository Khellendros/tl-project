class BookTranslator < ApplicationRecord
    belongs_to :user, foreign_key: :user_id
    belongs_to :book, foreign_key: :book_id
    belongs_to :translator_type, foreign_key: :translator_type_id

    validates :book_id, presence: true, on: %i[update create]
    validates :user_id, presence: true, on: %i[update create]
    validates :translator_type_id, presence: true, on: %i[update create]

    validates_uniqueness_of :user_id, scope: :book_id, on: %i[update create]

    attr_accessor :translator_email
end
