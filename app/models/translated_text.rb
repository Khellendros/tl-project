class TranslatedText < ApplicationRecord
    belongs_to :original_text, foreign_key: :original_text_id
    belongs_to :user, foreign_key: :user_id

    validates :original_text_id, presence: true, on: %i[update create]
    validates :user_id, presence: true, on: %i[update create]

    def is_creator?(_user_id)
        _user_id == user_id
    end

    def change_is_main
        if is_main
            translated_texts_ids = original_text.translated_texts.where.not(id: id).update_all(is_main: false)
        end
    end

    validate :original_text_blank?, on: %i[create update]

    def original_text_blank?
        errors.add(:original_text, :blank, message: 'can\'t be blank') if original_text.text.blank?
    end

end
