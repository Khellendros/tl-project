class SectionStatus < ApplicationRecord
    has_many :book_sections, foreign_key: :section_status_id

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]
end
