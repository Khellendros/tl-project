class BookSection < ApplicationRecord
    belongs_to :book, foreign_key: :book_id
    belongs_to :section_status, foreign_key: :section_status_id

    has_many :materials, -> { order 'position asc' }, foreign_key: :book_section_id, dependent: :destroy
    include Materialable

    validates :book_id, presence: true, on: %i[update create]
    validates :section_status_id, presence: true, on: %i[update create]
    validates :title, presence: true, on: %i[update create]
    validates :position, presence: true, on: %i[update create]

    validates_uniqueness_of :position, scope: :book_id, on: %i[update create]

    def is_translated?
        section_status_id == 1
    end

    def is_edited?
        section_status_id == 2
    end

    def is_completed?
        section_status_id == 3
    end
end
