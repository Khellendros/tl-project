class BookmarkStatus < ApplicationRecord
    has_many :user_bookmarks, foreign_key: :bookmark_status_id, dependent: :destroy

    validates :name, presence: true, on: %i[update create]

    validates_uniqueness_of :name, on: %i[update create]
end
