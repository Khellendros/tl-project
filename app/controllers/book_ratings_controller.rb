class BookRatingsController < ApplicationController
    def create
        ratings = BookRating.where(user_id: book_ratings_params[:user_id], book_id: book_ratings_params[:book_id])

        if ratings.blank?
            rating = BookRating.new(book_ratings_params)
            changed = if rating.save
                          true
                      else
                          false
                      end
        else
            rating = ratings.first
            changed = if rating.update(book_ratings_params.merge(id: rating.id))
                          true
                      else
                          false
                      end
        end

        flash.now[:notice] = if changed
                                 "Оценено в #{rating.value}"
                             else
                                 rating.errors.full_messages.join(', ')
                             end

        respond_to do |format|
            format.js
        end
    end

    def destroy; end

    private

    def book_ratings_params
        value = (params[:book_rating][:value].to_f * 2).to_i
        params.require(:book_rating).permit(:id, :user_id, :book_id, :value).merge(user_id: current_user.id, value: value)
    end
end
