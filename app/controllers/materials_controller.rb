class MaterialsController < ApplicationController
    before_action :get_book_section
    before_action :is_creator?

    def new
        @material = Material.new(book_section_id: @section.id)
        @position = 1
        materials = @section.materials.order(:position)
        @position += materials.last.position unless materials.blank?
    end

    def create
        @material_type = params[:material][:material_type]
        @material = if @material_type == 'text'
                        @section.original_texts.create(materials_params)
                    else
                        @section.original_images.create(materials_params)
                    end
        if @material.id.nil?
            flash.now[:notice] = @material.errors.full_messages.join(', ')
        else
            flash[:notice] = 'Фрагмент успешно добавлен'
            redirect_to(book_book_section_edit_list_path(@book.id, @section.id)) && return
        end
        respond_to do |format|
            format.js
        end
    end

    def update; end

    def destroy; end

    private

    def materials_params
        params.require(:material).permit(:position, :book_section_id)
    end

    def get_book_section
        @active_books = true
        @book = Book.find(params[:book_id])
        @section = @book.book_sections.find(params[:book_section_id])

        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil? || @section.nil?
    end

    def is_creator?
        # Разрешение на удаление, изменение, создание
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end

    def is_translator?
        translator = @book.book_translators.find_by(user_id: current_user.id)
        raise ActionController::RoutingError, with: -> { render_404 } if translator.nil?
    end
end
