class BookAuthorsController < ApplicationController
    before_action :get_book, only: %i[new create destroy]
    before_action :is_creator?, only: %i[new create destroy]

    def new
        @book_author = BookAuthor.new(book_id: @book.id)
        authors = Author.all.pluck(:name)
        @authors = authors.as_json(only: %i[id name])
    end

    def create
        @book_author = BookAuthor.new(book_authors_params)
        if @book_author.save
            flash[:notice] = 'Автор успешно добавлен'
            redirect_to(edit_book_path(@book.id)) && return
        else
            flash.now[:notice] = @book_author.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        @book_author = BookAuthor.find(params[:id])
        @id = @book_author.author_id
        if @book_author.destroy
            @notice_type = 'success'
            @book_author_destroy = true
            flash.now[:notice] = 'Автор успешно удален'
        else
            @notice_type = 'danger'
            @book_author_destroy = false
            flash.now[:notice] = @book_author.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    private

    def book_authors_params
        name = params[:book_author][:author_name]
        author_id = params[:book_author][:author_id]
        author_id = Author.find_or_create_by(name: name).id unless name.blank?
        params.require(:book_author).permit(:id, :book_id, :author_id).merge(author_id: author_id)
    end

    def get_book
        @active_books = true
        @book = Book.find(params[:book_id])
        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil?
    end

    def is_creator?
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end
end
