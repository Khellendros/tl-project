class BooksController < ApplicationController
    before_action :active_tab_books, only: %i[index edit]
    before_action :active_tab_library, only: %i[show read read_section]
    before_action :is_translator?, only: %i[translate]
    before_action :is_private?, only: %i[show read read_section]
    before_action :is_completed?, only: %i[read_section]
    before_action :is_creator?, only: %i[edit update destroy]
    before_action :get_selects, only: %i[edit new]

    def index
        @books = current_user.translated_books.paginate(page: params[:page], per_page: 24)
    end

    def show
        @bookmark_statuses = BookmarkStatus.all.order(:id)
    end

    def new
        @book = Book.new(user_id: current_user.id)
    end

    def create
        book = Book.new(book_params)
        if book.save
            flash[:notice] = 'Перевод успешно создан'
            redirect_to(edit_book_path(book.id)) && return
        else
            flash.now[:notice] = book.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def edit; end

    def update
        if @book.update(book_params)
            @notice_type = 'success'
            flash.now[:notice] = 'Изменения сохранены'
        else
            @notice_type = 'danger'
            flash.now[:notice] = @book.errors.full_messages.join(', ')
                             end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        if @book.destroy
            flash[:notice] = 'Перевод успешно удален'
            redirect_to(books_path) && return
        else
            flash.now[:notice] = @book.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def read
        @sections = @book.book_sections.order(:position)
    end

    def read_section; end

    private

    def active_tab_books
        @active_books = true
    end

    def active_tab_library
        @active_library = true
    end

    def get_selects
        @genres = Genre.all.order(:id)
        @book_statuses = BookStatus.all.order(:id)
    end

    def book_params
        book_genres_attributes = {}

        unless params[:book][:book_genres_attributes].nil?
            genre_ids = params[:book][:book_genres_attributes][:genre_ids]

            genre_ids.each_with_index do |genre_id, index|
                genre_book = BookGenre.find_by(genre_id: genre_id, book_id: params[:id])
                if genre_book.nil?
                    book_genres_attributes[index.to_s] = { genre_id: genre_id, book_id: params[:id], id: '' }
                else
                    book_genres_attributes[index.to_s] = { genre_id: genre_id, book_id: params[:id], id: genre_book.id }
                end
            end
        end

        if params[:book][:change_book_genres]
            destroy_book_genres = BookGenre.where(book_id: params[:id])
            destroy_book_genres = destroy_book_genres.where('genre_id NOT IN (?)', genre_ids) unless genre_ids.blank?

            book_genres_count = book_genres_attributes.count

            destroy_book_genres.each_with_index do |destroy_book_genre, index|
                book_genres_attributes[(book_genres_count.to_i + index + 1).to_s] = { genre_id: destroy_book_genre.genre_id, book_id: params[:id], id: destroy_book_genre.id, _destroy: true }
            end
        end

        params.require(:book).permit(:id, :title, :annotation, :user_id, :is_private, :start_date, :end_date, :book_status_id, :covers, book_genres_attributes: %i[id book_id genre_id]).merge(book_genres_attributes: book_genres_attributes)
    end

    def is_translator?
        # разрешение на редактирование
        @book = Book.find(params[:id])
        raise ActionController::RoutingError, with: -> { render_404 } unless @book.translator_ids.include?(current_user.id)
    end

    def is_creator?
        # разрешение на удаление
        @book = Book.find(params[:id])
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end

    def is_private?
        # разрешение на просмотр
        book_id = params[:book_id].nil? ? params[:id] : params[:book_id]
        @book = Book.find(book_id)
        raise ActionController::RoutingError, with: -> { render_404 } if @book.is_private && !@book.translator_ids.include?(current_user.id)
    end

    def is_completed?
        # проверка на готовность главы к чтению
        @section = @book.book_sections.find(params[:section_id])
        raise ActionController::RoutingError, with: -> { render_404 } unless @section.is_completed?
    end
end
