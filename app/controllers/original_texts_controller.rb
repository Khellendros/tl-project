class OriginalTextsController < ApplicationController
    def update
        text = OriginalText.find(params[:id])
        flash.now[:notice] = if text.update(original_text_params)
                                 'Изменения сохранены'
                             else
                                 text.errors.full_messages.join(', ')
                             end
        respond_to do |format|
            format.js
        end
    end

    private

    def original_text_params
        params.require(:original_text).permit(:id, :position, :book_section_id, :text)
    end
end
