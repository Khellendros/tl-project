class BookSectionsController < ApplicationController
    before_action :get_book
    before_action :get_section, only: %i[list edit update destroy]
    before_action :is_translator?, only: %i[index list]
    before_action :is_creator?, only: %i[new edit create update destroy]

    def index
        @sections = @book.book_sections.order(:position)
    end

    def new
        @section = BookSection.new(book_id: @book.id)
        @section_statuses = SectionStatus.all
        @position = 1
        book_sections = @book.book_sections.order(:position)
        @position += book_sections.last.position unless book_sections.blank?
    end

    def create
        @section = BookSection.new(book_sections_params)
        if @section.save
            flash[:notice] = 'Новая глава успешно добавлена'
            redirect_to(book_book_sections_path(@book.id)) && return
        else
            flash.now[:notice] = @section.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def list; end

    def edit
        @section_statuses = SectionStatus.all
        @position = @section.position
    end

    def update
        if @section.update(book_sections_params)
            flash.now[:notice] = 'Изменения сохранены'
            redirect_to(book_book_sections_path) && return
        else
            flash.now[:notice] = @section.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        if @section.destroy
            flash.now[:notice] = 'Раздел успешно удален'
            redirect_to(book_book_sections_path) && return
        else
            flash.now[:notice] = @section.errors.full_messages.join(', ')
          end
        respond_to do |format|
            format.js
        end
    end

    private

    def book_sections_params
        params.require(:book_section).permit(:id, :title, :book_id, :section_status_id, :position)
    end

    def get_book
        @active_books = true
        @book = Book.find(params[:book_id])
        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil?
    end

    def is_translator?
        raise ActionController::RoutingError, with: -> { render_404 } unless @book.translator_ids.include?(current_user.id)
    end

    def get_section
        section_id = params[:book_section_id].nil? ? params[:id] : params[:book_section_id]
        @section = BookSection.find(section_id)
    end

    def is_creator?
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end
end
