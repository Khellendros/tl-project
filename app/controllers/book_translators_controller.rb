class BookTranslatorsController < ApplicationController
    before_action :get_book, only: %i[new create destroy]
    before_action :is_creator?, only: %i[new create destroy]

    def new
        @book_translator = BookTranslator.new(book_id: @book.id)
        @translator_types = TranslatorType.where.not(id: 1)
    end

    def create
        @book_translator = BookTranslator.new(book_translators_params)
        if @book_translator.save
            flash[:notice] = 'Переводчик успешно добавлен'
            redirect_to(edit_book_path(@book.id)) && return
        else
            flash.now[:notice] = @book_translator.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        @book_translator = BookTranslator.find(params[:id])
        @id = @book_translator.user_id
        if @book_translator.destroy
            @notice_type = 'success'
            @book_translator_destroy = true
            flash.now[:notice] = 'Переводчик успешно удален'
        else
            @notice_type = 'danger'
            @book_translator_destroy = false
            flash.now[:notice] = @book_translator.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    private

    def book_translators_params
        user = User.find_by(email: params[:book_translator][:translator_email])
        user_id = nil
        user_id = user.id unless user.nil?
        params.require(:book_translator).permit(:id, :user_id, :book_id, :translator_type_id).merge(user_id: user_id)
    end

    def get_book
        @active_books = true
        @book = Book.find(params[:book_id])
        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil?
    end

    def is_creator?
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end
end
