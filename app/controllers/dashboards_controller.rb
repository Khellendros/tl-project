class DashboardsController < ApplicationController
    def index
        @books = Book.new_on_site
    end

    def library
        @active_library = true
        @genre_id = params[:genre_id]
        @order_by = params[:order_by]

        @genres = Genre.all.order(:id)
        @books = Book.library_book(@genre_id, @order_by).paginate(page: params[:page], per_page: 24)
    end

    def new
        @books = Book.all.order('created_at DESC').paginate(page: params[:page], per_page: 24)
    end

    def news; end
end
