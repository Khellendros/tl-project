class TranslatedTextsController < ApplicationController
    before_action :get_book_section
    before_action :is_translator?, only: %i[create show]
    before_action :get_translated_text, only: %i[show update destroy]
    before_action :is_creator_or_book_creator?, only: %i[update destroy]

    def create
        translated_text = TranslatedText.new(user_id: current_user.id, original_text_id: params[:original_text_id])
        if translated_text.save
            flash[:notice] = 'Вариант перевода успешно добавлен'
            redirect_to(book_book_section_translated_text_path(@book.id, @section.id, translated_text.id)) && return
        else
            flash.now[:notice] = translated_text.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def show; end

    def update
        if @translated_text.update(translated_text_params)
            @translated_text.change_is_main
            flash.now[:notice] = 'Изменения сохранены'
        else
            flash.now[:notice] = @@translated_text.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        if @translated_text.destroy
            flash[:notice] = 'Вариант перевода успешно удален'
            redirect_to(book_book_section_edit_list_path(@book.id, @section.id)) && return
        else
            flash.now[:notice] = @@translated_text.errors.full_messages.join(', ')
        end
        espond_to(&:js)
    end

    private

    def translated_text_params
        params.require(:translated_text).permit(:id, :original_text_id, :user_id, :text, :is_main)
    end

    def get_book_section
        @active_books = true
        @book = Book.find(params[:book_id])
        @section = @book.book_sections.find(params[:book_section_id])

        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil? || @section.nil?
    end

    def is_translator?
        raise ActionController::RoutingError, with: -> { render_404 } unless @book.translator_ids.include?(current_user.id)
    end

    def get_translated_text
        @translated_text = TranslatedText.find(params[:id])
        raise ActionController::RoutingError, with: -> { render_404 } if @translated_text.nil?
    end

    def is_creator_or_book_creator?
        raise ActionController::RoutingError, with: -> { render_404 } unless @translated_text.is_creator?(current_user.id) || @book.is_creator?(current_user.id)
    end
end
