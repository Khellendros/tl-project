class BookGenresController < ApplicationController
    before_action :get_book, only: %i[new create destroy]
    before_action :is_creator?, only: %i[new create destroy]

    def new
        @book_genre = BookGenre.new(book_id: @book.id)
        genres = Genre.all.pluck(:name)
        @genres = genres.as_json(only: %i[id name])
    end

    def create
        @book_genre = BookGenre.new(book_genres_params)
        if @book_genre.save
            flash[:notice] = 'Жанр успешно добавлен'
            redirect_to(edit_book_path(@book.id)) && return
        else
            flash.now[:notice] = @book_genre.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    def destroy
        @book_genre = BookGenre.find(params[:id])
        @id = @book_genre.genre_id
        if @book_genre.destroy
            @notice_type = 'success'
            @book_genre_destroy = true
            flash.now[:notice] = 'Жанр успешно удален'
        else
            @notice_type = 'danger'
            @book_genre_destroy = false
            flash.now[:notice] = @book_genre.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    private

    def book_genres_params
        name = params[:book_genre][:genre_name]
        genre_id = params[:book_genre][:genre_id]
        genre_id = Genre.find_by(name: name).id unless name.blank?
        params.require(:book_genre).permit(:id, :book_id, :genre_id).merge(genre_id: genre_id)
    end

    def get_book
        @active_books = true
        @book = Book.find(params[:book_id])
        raise ActionController::RoutingError, with: -> { render_404 } if @book.nil?
    end

    def is_creator?
        raise ActionController::RoutingError, with: -> { render_404 } if @book.user_id != current_user.id
    end
end
