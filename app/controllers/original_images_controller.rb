class OriginalImagesController < ApplicationController
    def update
        image = OriginalImage.find(params[:id])
        flash.now[:notice] = if image.update(original_image_params)
                                 'Изменения сохранены'
                             else
                                 image.errors.full_messages.join(', ')
                             end
        respond_to do |format|
            format.js
        end
    end

    private

    def original_image_params
        params.require(:original_image).permit(:id, :position, :book_section_id, :images)
    end
end
