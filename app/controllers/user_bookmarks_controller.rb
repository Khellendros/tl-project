class UserBookmarksController < ApplicationController
    def index
        @active_bookmark = true
        @user = User.find(params[:user_id])

        sql_text = ''
        sql_text += 'SELECT books.id AS book_id, books.title AS book_title, bookmark_statuses.name AS bookmark_status_name, bookmark_statuses.id as bookmark_status_id FROM books '
        sql_text += 'LEFT JOIN user_bookmarks ON user_bookmarks.book_id = books.id '
        sql_text += 'LEFT JOIN bookmark_statuses ON bookmark_statuses.id = user_bookmarks.bookmark_status_id '
        sql_text += "WHERE user_bookmarks.user_id = #{@user.id}"

        @bookmarks = ActiveRecord::Base.connection.exec_query(sql_text).to_hash
        @bookmarks = @bookmarks.sort_by { |bookmark| bookmark['bookmark_status_id'] }.group_by { |bookmark| bookmark['bookmark_status_name'] } unless @bookmarks.blank?
    end

    def create
        user_bookmark = UserBookmark.where(user_id: user_bookmarks_params[:user_id], book_id: user_bookmarks_params[:book_id])

        if user_bookmark.blank?
            user_bookmark = UserBookmark.new(user_bookmarks_params)
            @changed = if user_bookmark.save
                           true
                       else
                           false
              end
        else
            user_bookmark = user_bookmark.first
            @changed = if user_bookmark.update(user_bookmarks_params.merge(id: user_bookmark.id))
                           true
                       else
                           false
              end
        end

        if @changed
            @bookmark_status = user_bookmark.bookmark_status.name
            flash.now[:notice] = "Добавлено в #{@bookmark_status}"
        else
            flash.now[:notice] = user_bookmark.errors.full_messages.join(', ')
        end

        respond_to do |format|
            format.js
        end
    end

    def destroy; end

    private

    def user_bookmarks_params
        params.require(:user_bookmark).permit(:id, :user_id, :book_id, :bookmark_status_id).merge(user_id: current_user.id)
    end
end
