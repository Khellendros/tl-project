class UsersController < ApplicationController
    before_action :active_users
    before_action :check_user, only: %i[edit update]

    def index; end

    def show
        @user = User.find(params[:id])
    end

    def edit
        @genders = Gender.all.order(:id)
    end

    def update
        if @user.update(user_params)
            flash[:notice] = 'Изменения сохранены'
            redirect_to(user_path(@user.id)) && return
        else
            flash.now[:notice] = @user.errors.full_messages.join(', ')
        end
        respond_to do |format|
            format.js
        end
    end

    private

    def user_params
        params.require(:user).permit(:id, :name, :birthday, :gender_id, :avatars)
    end

    def active_users
        @active_users = true
    end

    def check_user
        @user = User.find(params[:id])
        raise ActionController::RoutingError, with: -> { render_404 } if @user != current_user
    end
end
